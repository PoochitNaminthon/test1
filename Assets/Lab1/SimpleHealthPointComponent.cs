using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleHealthPointComponent : MonoBehaviour
{
    [SerializeField] 
    private const float MAX_HP = 100;
    
    [SerializeField]
    private float _healthPoint;

    public float HealthPoint
    {
        get
        {
            return _healthPoint;
        }
        set
        {
            if (value > 0)
            {
                if (value <= MAX_HP)
                {
                    _healthPoint = value;
                }
                else
                {
                    print('h');
                    _healthPoint = MAX_HP;
                }
            }
        }
    }
    void Start()
    {
        
    }

    
    void Update()
    {
        print(_healthPoint);
    }
}
