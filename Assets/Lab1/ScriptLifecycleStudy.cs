using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptLifecycleStudy : MonoBehaviour
{
    private void Awake()
    {
        Debug.Log("Awake() has been called.");
    }

    private void Start()
    {
        Debug.Log("Start() has been called.");
    }

    private void OnDisable()
    {
        print("onDisable() has been called.");
    }

    private void OnDestroy()
    {
        print("onDestroy() has been called.");
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        print("onApplicationPause() has been called.");
    }
    
    private void OnApplicationQuit()
    {
        print("onApplicationQuit() has been called.");
    }
}
